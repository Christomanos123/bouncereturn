using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpAuto : MonoBehaviour
{
    [SerializeField] LayerMask m_GroundLayerMask;
    Rigidbody2D m_RigidBody;
    CircleCollider2D m_CircleCollider2D;

    [Range(0, 20)]
    [SerializeField] int force;

    //Has to touch the ground in order to jump
    WaitForSeconds m_WaiForSeconds;
    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_CircleCollider2D = GetComponent<CircleCollider2D>();
        m_WaiForSeconds = new WaitForSeconds(3.0f);
        StartCoroutine(JumpCycle());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }

    IEnumerator JumpCycle()
    {
        while (true)
        {
            yield return m_WaiForSeconds;
            Jump();
            yield return m_WaiForSeconds;
            
        }
    }


    void Jump()
    {
        float y = Input.GetAxisRaw("Vertical");

        //if (isGrounded())
        //{
            m_RigidBody.AddForce(force * Vector2.up, ForceMode2D.Impulse);
        //}
    }

    private bool isGrounded()
    {
        float extraHightTest = 0.1f;
        RaycastHit2D raycastHit2D = Physics2D.Raycast(m_CircleCollider2D.bounds.center, Vector2.down, m_CircleCollider2D.bounds.extents.y + extraHightTest, m_GroundLayerMask);
        return raycastHit2D.collider != null;
    }
}
