using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

//The Game manager script is responsible for managing level transitions and UI such as the play button on the main menu

public class GameManager : MonoBehaviour
{
    [SerializeField] Animator m_Transition;
    [SerializeField] float m_TransitionTime;
   
    AudioManager m_AudioManager;


    private void Start()
    {
        m_AudioManager = AudioManager.instance;
        if (m_AudioManager == null)
        {
            Debug.LogError("No audiomanager found in the scene");
        }
    }

    public void LoadFollowingLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void ReturnToMainMenu()
    {
        m_AudioManager.PlaySound("ReturnButton");
        StartCoroutine(LoadLevel(0));
    }
    IEnumerator LoadLevel(int levelIndex)
    {
        m_Transition.SetTrigger("Start");

        yield return new WaitForSeconds(m_TransitionTime);

        SceneManager.LoadScene(levelIndex);
    }

    public void RestartLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex));
    }

    public void PlayButton()
    {
        m_AudioManager.PlaySound("PlayButton");
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
 
}
