using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The laser script is responsible for activating and deactivating its child objects based on a random range of seconds
public class Laser : MonoBehaviour
{
    [SerializeField] GameObject m_Laser;
    [SerializeField] GameObject m_Light2D;
    
    [SerializeField] float m_FireRate = 2;

    WaitForSeconds m_WaiForSeconds;
    CapsuleCollider2D m_CapsuleCollider;

    void Start()
    {
        //m_FireRate = Random.Range(3,5);
        m_WaiForSeconds = new WaitForSeconds(m_FireRate);
        m_CapsuleCollider = GetComponent<CapsuleCollider2D>();
        StartCoroutine(LaserCycle());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Kill player
        if (collision.CompareTag("Player"))
        {
            AudioManager.instance.PlaySound("PlayerDeath");
            PlayerDeath playerDeath = collision.GetComponent<PlayerDeath>();
            playerDeath.Die();
        }
        
    }

    IEnumerator LaserCycle()
    {
        while (true)
        {
            Enable();
            AudioManager.instance.PlaySound("Laser");
            yield return m_WaiForSeconds;
            Disable();
            AudioManager.instance.StopSound("Laser");
            yield return m_WaiForSeconds;
        }
    }


    void Enable()
    {
        m_Laser.SetActive(true);
        m_Light2D.SetActive(true);

        //Enable collider
        m_CapsuleCollider.enabled = true;
    }

    void Disable()
    {
        m_Laser.SetActive(false);
        m_Light2D.SetActive(false);

        //Disable collider
        m_CapsuleCollider.enabled = false;
    }





}
