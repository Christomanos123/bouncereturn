using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
    [SerializeField] Counter m_Counter;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Add a life to player
        if (collision.CompareTag("Player"))
        {
            m_Counter.AddALife();
            AudioManager.instance.PlaySound("Life");
            gameObject.SetActive(false);
        }
    }
}
