using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{

    [SerializeField] GameManager m_GameManager;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Play complete level sound
        AudioManager.instance.PlaySound("Goal");

        //Call Game manager for next level
        m_GameManager.LoadFollowingLevel();
    }
}
