using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour
{

    [SerializeField] SpriteRenderer m_OutterRing;
    [SerializeField] SpriteRenderer m_InnerRing;
    [SerializeField] GameObject m_OutterLight;
    [SerializeField] GameObject m_InnerLight;
    [SerializeField] Counter m_Counter;

    bool m_Played = false;

    private void OnTriggerExit2D(Collider2D collision)
    {
        m_Counter.RemoveARing();
        ChangeColour();

        if (m_Played == false)
        {
            AudioManager.instance.PlaySound("Ring");
            m_Played = true;
        }

    }


    void ChangeColour()
    {
       
        Color grey = new Color(159.0f / 255.0f, 159.0f / 255.0f, 159.0f / 255.0f);
        m_InnerRing.color = grey;
        m_OutterRing.color = grey;

        m_OutterLight.SetActive(false);
        m_InnerLight.SetActive(false);

    }
}
