using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Counter is responsible for holding the number of lives and rings left in the scene. Also, updates the UI. 

public class Counter : MonoBehaviour
{

    [SerializeField] List<Sprite> m_NumberSprites;
    [SerializeField] Image m_LivesSprite;
    [SerializeField] Image m_RingsSprite;

    [SerializeField] int m_Lives;
    [SerializeField] int m_RingsCounter;


    // Start is called before the first frame update
    void Start()
    {
        m_LivesSprite.sprite = m_NumberSprites[m_Lives];
        m_RingsSprite.sprite = m_NumberSprites[m_RingsCounter];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void AddALife()
    {
        m_Lives++;
        m_LivesSprite.sprite = m_NumberSprites[m_Lives];
    }


   public void RemoveARing()
    {

        if (m_RingsCounter <= 0)
        {
            m_RingsCounter = 0;
            m_RingsSprite.sprite = m_NumberSprites[m_RingsCounter];
        }
        else
        {
            m_RingsCounter--;
            m_RingsSprite.sprite = m_NumberSprites[m_RingsCounter];
        }


       
    }


}
