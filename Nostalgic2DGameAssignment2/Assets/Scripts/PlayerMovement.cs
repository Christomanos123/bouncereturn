using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D m_RigidBody;


    [Range(0,5)]
    [SerializeField] int speed;
    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       MovePlayerVelocity();
    }

    void MovePlayerVelocity()
    {
        float x = Input.GetAxisRaw("Horizontal");

        m_RigidBody.velocity = new Vector3(x * speed, m_RigidBody.velocity.y); // Move to the right
        
    }

}
