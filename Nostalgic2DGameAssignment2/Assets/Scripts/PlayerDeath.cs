using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] Sprite m_DeathSprite;
    [SerializeField] GameManager m_GameManager;

    Rigidbody2D m_RigidBody;
    SpriteRenderer m_SpriteRenderer;
    PlayerMovement m_PlayerMovement;

    // Start is called before the first frame update
    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_PlayerMovement = GetComponent<PlayerMovement>();
    }

    

    public void Die()
    {
        //Disable input
        m_PlayerMovement.enabled = false;
        m_SpriteRenderer.sprite = m_DeathSprite;
        m_RigidBody.velocity = new Vector2(0f, 0f);
        m_RigidBody.angularVelocity = 0;
        m_RigidBody.isKinematic = true;
        m_GameManager.RestartLevel();
    }

}
